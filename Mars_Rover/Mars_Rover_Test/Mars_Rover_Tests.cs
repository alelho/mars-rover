﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mars_Rover;

namespace Mars_Rover_Test
{
    [TestClass]
    public class Mars_Rover_Tests
    {
        [TestMethod]
        public void Mars_Rover_Output_Test()
        {
            int x = 5;
            int y = 5;
            Position coor_upper_right = new Position(x+1, y+1, 0);

            var mars = new Mars
            {
                Board = new int[coor_upper_right.X, coor_upper_right.Y]
            };

            var rover1 = new Rover(1, "1 2 N", "LMLMLMLMM", coor_upper_right, mars.Board);
            var rover2 = new Rover(2, "3 3 E", "MMRMMRMRRM", coor_upper_right, mars.Board);

            rover1.Exec_Commands(mars.Board);
            Assert.AreEqual("1 3 N", rover1.ToString());

            rover2.Exec_Commands(mars.Board);
            Assert.AreEqual("5 1 E", rover2.ToString());
        }
    }
}
