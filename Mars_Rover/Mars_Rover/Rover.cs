﻿using System;

namespace Mars_Rover
{
    public class Rover
    {
        #region private variables

        private Position _coor_upper_right;
        private string _commands;

        #endregion


        #region public properties

        public int ID { get; }
        public Position Pos_Initial { get; }
        public Position Pos_Final { get; }
        public bool Error { get; private set; }

        #endregion


        #region constructor

        public Rover( int id, string pos, string commands, Position coor_upper_right, int[,] board )
        {
            ID = id;
            Error = false;
            _coor_upper_right = coor_upper_right;

            var coor = pos.Split( ' ' );

            int x = Convert.ToInt32( coor[0] );
            int y = Convert.ToInt32( coor[1] );

            if( ( x >= 0 && x < coor_upper_right.X ) && ( y >= 0 && y < coor_upper_right.Y ) && board[x, y] == 0 ) // check if is valid place to putting the rover
            {
                board[x, y] = ID; // Sets the place as occupied by this rover
                Pos_Initial = new Position( x, y, Cardinal_Point_Utils.GetValue( coor[2].ToString().ToUpper() ) );
                Pos_Final = new Position( Pos_Initial.X, Pos_Initial.Y, Pos_Initial.Cardinal_Point );
                _commands = commands;
            }
            else
            {
                Error = true;
                Console.WriteLine( string.Format("Rover {0}: Place occupied by another Rover or invalid place", ID) );
            }
        }
        #endregion


        #region public methods

        public void Exec_Commands(int[,] board )
        {
            foreach( var ele in _commands )
            {
                switch( ele.ToString().ToUpper() )
                {
                    case "M":
                        if( !Pos_Final.Move( board, _coor_upper_right ) )
                            Error = true;
                        break;
                    case "R":
                        Pos_Final.Turn_Right();
                        break;
                    case "L":
                        Pos_Final.Turn_Left();
                        break;
                    default:
                        break;
                }

                if( Error )
                    break;
            }
        }

        public override string ToString()
        {
            if(Error)
                return string.Format( "{0} {1} {2} Place occupied by another Rover or invalid place. Rover {3} has been stopped.", Pos_Final.X, Pos_Final.Y, Cardinal_Point_Utils.GetName( Pos_Final.Cardinal_Point ), ID );
            else
                return string.Format( "{0} {1} {2}", Pos_Final.X, Pos_Final.Y, Cardinal_Point_Utils.GetName( Pos_Final.Cardinal_Point ) );
        }
        #endregion


        #region private methods

        #endregion
    }
}
