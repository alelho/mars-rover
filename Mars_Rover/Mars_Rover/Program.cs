﻿/********************************************************************************************
* Application developed for a job opportunity, but the company canceled the hiring process. *
* Date: 11/2016																			    *
* Last revision: 09/2018																    *
********************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;

namespace Mars_Rover
{
    class Program
    {
        static public void Main( string[] args )
        {
            var mars = new Mars();
            mars.Init();

            Console.ReadKey();
        }
    }

    public class Mars
    {
        #region constants

        const string PATH = @"input.txt";

        #endregion


        #region Properties

        public IList<Rover> Rovers { get; set; }
        public int[,] Board { get; set; } = null;

        public Position Coor_upper_right;

        #endregion

        public Mars()
        {
            this.Rovers = new List<Rover>();        
        }

        public void Init()
        {
            if (!Read_Input_File())
            {
                Console.WriteLine("Input file not found or Input file incorrect format.");
                Console.WriteLine("The program has exited.");
                Console.ReadKey();
                System.Environment.Exit(1);
            }

            Exec_Rovers(this.Rovers);
        }

        private bool Read_Input_File()
        {
            if (!File.Exists(PATH))
                return false;
            try
            {
                using (StreamReader sr = new StreamReader(PATH))
                {
                    string pos;
                    int cont = 1;

                    /**** Get upper-right coordinates *****/
                    pos = sr.ReadLine();
                    var coor = pos.Split(' ');
                    int x = Convert.ToInt32(coor[0]) + 1;
                    int y = Convert.ToInt32(coor[1]) + 1;
                    Coor_upper_right = new Position(x, y, 0);
                    Board = new int[x, y];
                    /*************************************/

                    /**** Get position and commands from each rover *****/
                    while ((pos = sr.ReadLine()) != null)
                    {
                        if (pos != string.Empty)
                        {
                            string commands = sr.ReadLine();
                            this.Rovers.Add(new Rover(cont, pos, commands, Coor_upper_right, Board));

                            cont++;
                        }
                    }
                    /***************************************************/
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private void Exec_Rovers(IList<Rover> rovers)
        {
            foreach (Rover rover in rovers)
            {
                if (!rover.Error)
                {
                    rover.Exec_Commands(Board);
                    Console.WriteLine(rover.ToString());
                }
            }
        }

    }
}
