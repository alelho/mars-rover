﻿using System;

namespace Mars_Rover
{
    public class Position
    {
        #region constants

        int CARDINAL_POINT_MAX = 4;
        int CARDINAL_POINT_MIN = 1;

        #endregion


        #region public properties

        public int X { get; private set; }
        public int Y { get; private set; }
        public int Cardinal_Point { get; private set; }

        #endregion


        #region constructor

        public Position( int x, int y, int cp )
        {
            X = x;
            Y = y;
            Cardinal_Point = cp;
        }

        #endregion


        #region public methods

        public void Turn_Left()
        {
            Cardinal_Point = Cardinal_Point == CARDINAL_POINT_MIN ? CARDINAL_POINT_MAX : Cardinal_Point - 1;
        }

        public void Turn_Right()
        {
            Cardinal_Point = Cardinal_Point == CARDINAL_POINT_MAX ? CARDINAL_POINT_MIN : Cardinal_Point + 1;
        }

        public bool Move( int[,] board, Position coor_upper_right )
        {
            int aux_y, aux_x;

            switch (Cardinal_Point)
            {
                case (int)CardinalPoint.North:
                    aux_y = Y + 1;

                    if (aux_y < coor_upper_right.Y && board[X, aux_y] == 0)
                    {
                        board[X, aux_y] = board[X, Y]; // Sets the place as occupied;
                        board[X, Y] = 0; // Sets the previuos place as free
                        Y = aux_y;

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                case (int)CardinalPoint.South:
                    aux_y = Y - 1;

                    if (aux_y >= 0 && board[X, aux_y] == 0)
                    {
                        board[X, aux_y] = board[X, Y]; // Sets the place as occupied;
                        board[X, Y] = 0; // Sets the previuos place as free
                        Y = aux_y;

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                case (int)CardinalPoint.East:
                    aux_x = X + 1;

                    if (aux_x < coor_upper_right.X && board[aux_x, Y] == 0)
                    {
                        board[aux_x, Y] = board[X, Y]; // Set the place as occupied;
                        board[X, Y] = 0; // Sets the previuos place as free
                        X = aux_x;

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                case (int)CardinalPoint.West:
                    aux_x = X - 1;

                    if (aux_x >= 0 && board[aux_x, Y] == 0)
                    {
                        board[aux_x, Y] = board[X, Y]; // Set the place as occupied;
                        board[X, Y] = 0; // Sets the previuos place as free
                        X = aux_x;

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    Console.WriteLine( "Invalid Cardinal Point" );
                    return true;
            }
        }

        #endregion

        #region private methods

        #endregion
    }
}
