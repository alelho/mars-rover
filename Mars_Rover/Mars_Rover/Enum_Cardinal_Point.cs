﻿using System.Collections.Generic;
using System.Linq;

namespace Mars_Rover
{
    public enum CardinalPoint
    {
        None = 0,
        North,
        East,
        South,
        West
    }

    public static class Cardinal_Point_Utils
    {
        public static Dictionary<string, int> Data
        {
            get
            {
                return new Dictionary<string, int>()
                {
                    { "None",  (int)CardinalPoint.None },
                    { "N", (int)CardinalPoint.North },
                    { "E", (int)CardinalPoint.East },
                    { "S", (int)CardinalPoint.South },
                    { "W", (int)CardinalPoint.West }
                };
            }
        }

        public static string GetName( CardinalPoint data )
        {
            return GetName( (int)data );
        }
        public static string GetName( int value )
        {
            if( Cardinal_Point_Utils.Data.ContainsValue( value ) )
            {
                return Cardinal_Point_Utils.Data.Where( d => d.Value == value ).First().Key;
            }
            return string.Empty;
        }

        public static int GetValue( string name )
        {
            if( Cardinal_Point_Utils.Data.ContainsKey( name ) )
            {
                return Cardinal_Point_Utils.Data[name];
            }
            return -1;
        }
    }
}
